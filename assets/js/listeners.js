// Listeners

// changing states
document.querySelector('.quest_result .btn_style').addEventListener('click', function() {
    numQuestion++;
    displayQuestion(numQuestion);
});

document.querySelector('.result .btn_style').addEventListener('click', function() {
    result.hidden = true;
    intro.hidden = false;
    numQuestion = 0;
});

buttonIntro.addEventListener("click", function(e) {
    e.preventDefault();
    if (e.target.tagName === 'A') {
        intro.hidden = true;
        test.hidden = false;
        displayQuestion(numQuestion);
    }
});

// Button Listeners


answBtnBox.addEventListener('click', function(e) {
    e.preventDefault();
    if (numQuestion === 7) {
        initEnd();
    } else {
        if (document.querySelector('.active')) {
            showResultAnswer(numQuestion);
        }
    }
});

// close link

icoClose.addEventListener('click', function() {
    numQuestion = 0;
    result.hidden = true;
    intro.hidden = false;
    test.hidden = true;
});
