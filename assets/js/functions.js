function displayQuestion(num) {
    questBox.hidden = false;
    questResult.hidden = true;
    if (document.querySelector(".test .active")) {
        document.querySelector(".test .active").classList.remove('active');
    }
    questHead.textContent = questions[num][0];

    for (var i = 0; i < answTxt.length; i++) {
        answTxt[i].children[1].textContent = answers[num][i];
        answTxt[i].addEventListener('click', function(e) {
            e.preventDefault();
            checkAnswer(numQuestion, e.currentTarget);
        });
    }
}


function checkAnswer(num, el) {
    if (document.querySelector(".test .active")) {
        document.querySelector(".test .active").classList.remove('active');
    }
    el.classList.add('active');
}

function showResultAnswer(num) {
    questBox.hidden = true;
    questResult.hidden = false;
    var answer = document.querySelector('.active');
    if (answers[num][questions[num][1]] === answer.textContent) {
        otvet.textContent = "Правильно!";
        otvet.classList.add('yes');
    } else {
        otvet.classList.remove('yes');
        otvet.textContent = "Неправильно";
    }
}


function initEnd() {
    test.hidden = true;
    result.hidden = false;
}

