var intro = document.querySelector(".intro");
var test = document.querySelector(".test");
var result = document.querySelector(".result");
var questBox = document.querySelector(".quest_box");
var questResult = document.querySelector(".quest_result");
var questHead = document.querySelector(".quest_head");
var answBtnBox = document.querySelector(".answ_btn_box .btn_style");
var answTxt = document.querySelectorAll(".answ_txt");
var otvet = document.querySelector(".otvet");
var buttonIntro = document.querySelector(".intro .btn_box_bot");
var icoClose = document.querySelector(".ico_close");

test.hidden = true;
result.hidden = true;
questBox.hidden = true;
questResult.hidden = true;
var numQuestion = 0;

var questions = [["Во сколько приедешь на собеседование?", 1],
    ["Что наденешь на встречу?", 0],
    ["Что скажешь рекрутеру при встрече?", 1],
    ["Вы с HR-менеджером сидите друг напротив друга. Искра, буря, эмоция...", 1],
    ["Самый важный вопрос — обсуждение зарплаты.", 1],
    ["Спросили про опыт. Это же моя первая работа! Что говорить?", 0],
    ["Мои сильные и слабые стороны?", 1],
    ["После собеседования обещали позвонить или написать. Но ничего не происходит...", 0]];

var answers = [[
    "Я всегда опаздываю. С этим ничего не поделать.",
    "Всегда приезжаю за 15 минут, чтобы успокоиться самому и не бесить других."
],
    [
        "Оденусь так же, как и всегда: аккуратно. Ни к чему лишнее неудобство.",
        "Что лежит на стуле? Подойдет!"
    ],
    [
        "Добрый день! Хотя какой он добрый, если я приехал к вам на собеседование?",
        "Добрый день! Приятно познакомится с вами лично!"
    ],
    [
        "Буду молчать. Пусть спрашивают меня. А вообще я все в резюме написал.",
        "Неловкая пауза? Ну уж нет. Мне тоже есть что спросить у будущего работодателя."
    ],
    [
        "Скажу, что ничего не знаю. Пусть сами назовут сумму.",
        "Я изучил рынок и примерно понимаю, чего я хочу. Могу высказаться первым."
    ],
    [
        "Тогда буду рассказывать про студенческие проекты, они мне кажутся интересными.",
        "Это же моя первая работа. Вы чего?"
    ],
    [
        "Я амбициозный перфекционист и трудоголик. Какие могут быть слабые стороны?",
        "Буду отвечать объективно: я хорош, но мне есть над чем поработать."
    ],
    [
        "Тактично напишу им. Мне нужно знать любое решение, которое принял работодатель.",
        "Я хоть и младший сотрудник, но гордый. Не буду сам писать ни за что!"
    ],];